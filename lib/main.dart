import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:resapp/api/api_login.dart';
import 'package:resapp/api/firebase_detail_restaurant.dart';
import 'package:resapp/api/firebase_list_restaurant.dart';
import 'package:resapp/api/api_get_position.dart';
import 'package:resapp/bloc/add_restaurant_bloc.dart';
import 'package:resapp/bloc/detail_restaurant_bloc.dart';
import 'package:resapp/bloc/googlemap_bloc.dart';
import 'package:resapp/bloc/home_bloc.dart';
import 'package:resapp/bloc/home_fb_bloc.dart';
import 'package:resapp/bloc/login_bloc.dart';

import 'login_screen/login_screen.dart';
import 'package:firebase_core/firebase_core.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          Provider<ApiService>(
            create: (context) => ApiService(),
          ),
          Provider<GetNameApi>(
            create: (context) => GetNameApi(),
          ),
          Provider<GetDataFB>(
            create: (context) => GetDataFB(),
          ),
          Provider<GetLL>(
            create: (context) => GetLL(),
          ),
          Provider<AddRestaurantBloC>(
            create: (context) => AddRestaurantBloC(),
          ),
          Provider<GetDataReview>(
            create: (context) => GetDataReview(),
          ),
          ProxyProvider<GetLL, GMBloC>(
              update: (context, api, bloc) => GMBloC(getLL: api)),
          ProxyProvider<GetDataReview, DetailBloC>(
              update: (context, fb, bloc) => DetailBloC(getDataReview: fb)),
          ProxyProvider<ApiService, LoginBloC>(
              update: (context, api, bloc) => LoginBloC(api)),
          ProxyProvider<GetNameApi, HomeBloC>(
              update: (context, api, bloc) => HomeBloC(getNameApi: api)),
          ChangeNotifierProxyProvider<GetDataFB, HomeFbBloC>(
              create: (context) => HomeFbBloC(),
              update: (context, fb, bloc) => HomeFbBloC(getDataFB: fb)),
        ],
        child: MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'Flutter Demo',
            theme: ThemeData(
              primarySwatch: Colors.blue,
            ),
            home: LoginScreen()));
  }
}
