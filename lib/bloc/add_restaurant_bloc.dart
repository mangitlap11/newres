import 'dart:io';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:rxdart/subjects.dart';

class AddRestaurantBloC {
  var _image = BehaviorSubject<File>();
  Stream<File> get image => _image.stream;
  Future<String> getUrl() async {
    String downloadURL =
        await FirebaseStorage.instance.ref('restaurant.jpeg').getDownloadURL();
    return downloadURL;
  }

  void pushData(
      nameController, addressController, descriptionController, downloadURL) {
    final ref = FirebaseDatabase.instance.reference().child("List Restaurant");
    final refDetail =
        FirebaseDatabase.instance.reference().child("Restaurant Detail");

    ref.child("1").set({
      "Name": "${nameController.text}",
      "Adress": "${addressController.text}",
      "Description": "${descriptionController.text}",
      "ImagePath": "$downloadURL",
      "Save": false,
      "Rating": "4.2",
    });
    refDetail.child("1").set({
      "Avatar":
          "https://firebasestorage.googleapis.com/v0/b/restaurantapp-318515.appspot.com/o/man.jpg?alt=media&token=6015ae9e-d002-4d52-80e9-4327fd826c24",
      "Comment": "Ngon bổ rẻ ",
      "Like": "50",
      "Name": "Mẫn"
    });
  }

  Future getImagefromGallary() async {
    final picker = ImagePicker();
    final pickedImage = await picker.pickImage(source: ImageSource.gallery);
    _image.add(File(pickedImage.path));
  }

  Future uploadFile() async {
    File imageFile = File(_image.value.path);
    print(imageFile);
    await FirebaseStorage.instance.ref("restaurant.jpeg").putFile(imageFile);
  }

  depose() {
    _image.close();
  }
}
