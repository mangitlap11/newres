import 'package:resapp/api/api_login.dart';
import 'package:resapp/model/model_api.dart';
import 'package:rxdart/rxdart.dart';

class LoginBloC {
  final ApiService apiService;
  LoginBloC(this.apiService);

  var _eye = BehaviorSubject<bool>.seeded(false);
  Stream<bool> get eye => _eye.stream;
  var _isLogin = BehaviorSubject<bool>.seeded(false);
  Stream<bool> get isLogin => _isLogin.stream;
  // var _loading = BehaviorSubject<bool>.seeded(false);
  // Stream<bool> get Loading => _loading.stream;

  // void showLoading() {
  //   _loading.add(true); //show loading
  // }

  // void hideLoading() {
  //   _loading.add(false); //hide loading
  // }

  void hidePassword() {
    _eye.add(!_eye.value);
  }

  LoginRequestModel requestModel = LoginRequestModel();

  set email(String value) => requestModel.email = value;

  set password(String value) => requestModel.password = value;

  Future<LoginResponseModel> loginRequest(email, password) async {
    if (email != null && password != null) {
      LoginResponseModel result = await apiService.login(requestModel);
      return result;
    }
    throw Exception("Email or Password not true");
  }

  void depose() {
    _eye.close();
    _isLogin.close();
  }
}
