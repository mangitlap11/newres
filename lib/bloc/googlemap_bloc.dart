import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:resapp/api/api_get_position.dart';
import 'package:rxdart/subjects.dart';

class GMBloC {
  GetLL getLL;
  GMBloC({this.getLL});
  Future<dynamic> getLocation(String adress) async {
    dynamic resultGM = await getLL.getLL(adress);
    return resultGM;
  }

  var _myController = BehaviorSubject<GoogleMapController>();
  Stream<GoogleMapController> get myController => _myController.stream;
  var _maker = BehaviorSubject<Set<Marker>>();
  Stream<Set<Marker>> get maker => _maker.stream;
  void onAddMarker(lat, long) {
    Set<Marker> _markerFake = {};
    _markerFake.add(Marker(
      markerId: MarkerId(LatLng(lat, long).toString()),
      position: LatLng(lat, long),
    ));
    _maker.add(_markerFake);
  }

  void onMapCreated(GoogleMapController controller) {
    _myController.add(controller);
  }

  depose() {
    _myController.close();
  }
}
