import 'package:resapp/api/firebase_detail_restaurant.dart';

import 'package:resapp/model/review_model.dart';
import 'package:rxdart/subjects.dart';

class DetailBloC {
  GetDataReview getDataReview;
  DetailBloC({this.getDataReview});
  var _more = BehaviorSubject<bool>.seeded(false);
  Stream<bool> get more => _more.stream;
  var _review = BehaviorSubject<Review>();
  Stream<Review> get review => _review.stream;
  void setMore() {
    _more.add(!_more.value);
  }

  Future<Review> getReview(listsDetail, indexD) async {
    Review dataReview = await getDataReview.getDataDetail(listsDetail, indexD);
    _review.add(dataReview);
  }

  depose() {
    _review.close();
    _more.close();
  }
}
