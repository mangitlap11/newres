import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:resapp/api/firebase_list_restaurant.dart';
import 'package:resapp/model/list_restaurant_model.dart' as Model;
import 'package:rxdart/subjects.dart';

class HomeFbBloC extends ChangeNotifier {
  GetDataFB getDataFB;
  HomeFbBloC({this.getDataFB});
  var _lists = BehaviorSubject<List<Model.Event>>();
  Stream<List<Model.Event>> get lists => _lists.stream;
  final ref = FirebaseDatabase.instance.reference().child("List Restaurant");

  Future<List<Model.Event>> getDataFromFb() async {
    List<Model.Event> dataGetFromFb = await getDataFB.getData();
    _lists.add(dataGetFromFb);
  }

  void setSave(int i) {
    _lists.value[i].save = !_lists.value[i].save;
    _lists.add(_lists.value);
    ref.child(_lists.value[i].key).child("Save").set(_lists.value[i].save);
    notifyListeners();
  }

  depose() {
    _lists.close();
  }
}
