import 'package:resapp/api/api_login.dart';

import 'package:rxdart/rxdart.dart';

class HomeBloC {
  GetNameApi getNameApi;

  HomeBloC({this.getNameApi});
  var _save = BehaviorSubject<bool>();
  Stream<bool> get save => _save.stream;

  void changeSave() {
    _save.add(!_save.value);
  }

  Future<dynamic> getName(data) {
    dynamic result = getNameApi.getUserData(data);
    return result;
  }

  void depose() {
    _save.close();
  }
}
