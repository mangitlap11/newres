class Event {
  String key;

  String address;
  String name;
  String rating;
  String imagePath;
  String description;
  bool save;

  Event({
    this.key,
    this.address,
    this.name,
    this.description,
    this.rating,
    this.imagePath,
    this.save,
  });
}
