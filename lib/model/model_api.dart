class LoginResponseModel {
  final dynamic data;
  final String error;
  final String message;
  final bool success;
  LoginResponseModel({this.data, this.error, this.message, this.success});
  factory LoginResponseModel.fromJson(Map<String, dynamic> json) {
    return LoginResponseModel(
        data: json["data"] != null ? json["data"] : "",
        error: json["error"] != null ? json["error"] : "",
        message: json["message"] != null ? json["message"] : "",
        success: json["success"] != null ? json["success"] : "");
  }
}

class LoginRequestModel {
  String email;
  String password;
  LoginRequestModel({this.email, this.password});
  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = {
      'email': email.trim(),
      'password': password.trim(),
    };
    return map;
  }
}
