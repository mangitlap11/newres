import 'package:firebase_database/firebase_database.dart';
import 'package:resapp/model/review_model.dart';

class GetDataReview {
  final ref = FirebaseDatabase.instance.reference().child("Restaurant Detail");

  Future<Review> getDataDetail(listsDetail, indexD) async {
    Review detailReview = Review();
    await ref.child(listsDetail[indexD].key).once().then((DataSnapshot snap) {
      print("asdasdasd");
      print(snap);

      detailReview = new Review(
          key: snap.key,
          avatar: snap.value["Avatar"],
          comment: snap.value["Comment"],
          like: snap.value["Like"],
          name: snap.value["Name"]);
      print(detailReview.comment);
    });
    return detailReview;
  }
}
