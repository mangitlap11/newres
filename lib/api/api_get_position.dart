import 'package:http/http.dart' as http;
import 'dart:convert';

class GetLL {
  Future<dynamic> getLL(address) async {
    final queryParam = {
      "access_key": "3324331f9b6167f5b12e1b2401237eb5",
      "query": "$address"
    };
    final uri = Uri.http("api.positionstack.com", "/v1/forward", queryParam);
    http.Response response = await http.get(
      uri,
    );
    if (response.statusCode >= 200 && response.statusCode <= 299) {
      final jsonData = await json.decode(response.body)["data"];
      print("LAT LONG LAT LONG");
      print(jsonData);
      return jsonData;
    } else {
      throw Exception('Fail to request');
    }
  }
}
