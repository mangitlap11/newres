import 'package:resapp/model/model_api.dart';

import 'package:http/http.dart' as http;
import 'dart:convert';

class ApiService {
  //tâjp hợp các ham xu ly api
  Future<LoginResponseModel> login(
    LoginRequestModel requestModel,
  ) async {
    {
      String url = "http://103.92.29.62:7006/api/v1/auth/login";
      final response =
          await http.post(Uri.parse(url), body: requestModel.toJson());

      if (response.statusCode >= 200 && response.statusCode <= 299) {
        return LoginResponseModel.fromJson(json.decode(response.body));
      } else {
        return LoginResponseModel.fromJson(json.decode(response.body));
      }
    }
  }
}

class GetNameApi {
  Future<dynamic> getUserData(dynamic data) async {
    final String url = "http://103.92.29.62:7006/api/v1/auth/get-visa";
    http.Response response =
        await http.get(Uri.parse(url), headers: {"Authorization": "$data"});
    print(json.decode(response.body));

    if (response.statusCode >= 200 && response.statusCode <= 299) {
      final jsonData = await json.decode(response.body)["data"];
      print(jsonData);
      return jsonData;
    } else {
      throw Exception('Fail to request');
    }
  }

  void setState(Null Function() param0) {}
}
