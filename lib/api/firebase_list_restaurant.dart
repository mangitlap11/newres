import 'package:firebase_database/firebase_database.dart';

import 'package:resapp/model/list_restaurant_model.dart' as Model;

class GetDataFB {
  final ref = FirebaseDatabase.instance.reference().child("List Restaurant");

  Future<List<Model.Event>> getData() async {
    List<Model.Event> lists = [];

    await ref.once().then((DataSnapshot snap) {
      var data = snap.value;
      lists.clear();
      print(data);
      data.forEach((key, value) {
        Model.Event event = new Model.Event(
            key: key,
            address: value["Adress"],
            name: value["Name"],
            description: value["Description"],
            rating: value["Rating"],
            imagePath: value["ImagePath"],
            save: value["Save"]);
        print(event.address);
        lists.add(event);
        print(lists.first);
      });
    });
    return lists;
  }
}
