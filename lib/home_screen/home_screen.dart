import 'package:flutter/material.dart';
import 'package:resapp/bloc/home_fb_bloc.dart';
import 'package:resapp/home_screen/add_restaurant_screen/add_restaurant_screen.dart';
import 'package:resapp/home_screen/googlemap_screen/googlemap_screen.dart';
import 'package:resapp/home_screen/restaurant_detail_screen/restaurant_detail_screen.dart';
import 'package:resapp/model/list_restaurant_model.dart' as ModelEvent;
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  final dynamic data;
  const HomeScreen({this.data});

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  static const TextStyle optionStyle =
      TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: Colors.grey);

  @override
  void initState() {
    super.initState();
    var bloCFb = context.read<HomeFbBloC>();
    bloCFb.getDataFromFb().then((value) => print(value));
  }

  @override
  Widget build(BuildContext context) {
    var bloCFb = context.read<HomeFbBloC>();
    return Scaffold(
      backgroundColor: Color(0xffFAFBFF),
      appBar: appbar(),
      body: Padding(
          padding: const EdgeInsets.fromLTRB(16, 24, 16, 0),
          child: listRestaurant(bloCFb)),
      bottomNavigationBar: BottomNavigationBar(
          onTap: (index) {},
          showUnselectedLabels: true,
          showSelectedLabels: true,
          iconSize: 25,
          unselectedLabelStyle: optionStyle,
          selectedItemColor: Colors.blue,
          unselectedItemColor: Colors.grey,
          items: [
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.home,
                  color: Colors.blue,
                ),
                label: "Home",
                backgroundColor: Colors.white),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.search,
                  color: Colors.grey,
                ),
                label: "Discover",
                backgroundColor: Colors.white),
            BottomNavigationBarItem(
                icon: Icon(Icons.auto_stories, color: Colors.grey),
                label: "Bookmark",
                backgroundColor: Colors.white),
            BottomNavigationBarItem(
                icon: Icon(Icons.perm_contact_calendar_outlined,
                    color: Colors.grey),
                label: "Profile",
                backgroundColor: Colors.white),
          ]),
    );
  }

  Widget appbar() {
    return AppBar(
      elevation: 0,
      backgroundColor: Color(0xffFAFBFF),
      title: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 25),
        child: (Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text(
            "Hello Man ,",
            style: TextStyle(
                fontSize: 20, fontWeight: FontWeight.w600, color: Colors.black),
          ),
          RichText(
            text: TextSpan(
                style: TextStyle(
                  wordSpacing: 1,
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                ),
                children: [
                  TextSpan(
                    text: 'Explore',
                    style: TextStyle(color: Colors.black),
                  ),
                  TextSpan(
                    text: " ",
                  ),
                  TextSpan(
                      text: 'New Hotels', style: TextStyle(color: Colors.blue))
                ]),
          )
        ])),
      ),
      actions: [
        Padding(
          padding: const EdgeInsets.only(right: 18.0),
          child: TextButton(
              child: Container(
                  width: 30,
                  height: 30,
                  child: Image.asset(
                    "images/add.png",
                  )),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            AddHotelScreen(data: widget.data)));
              }),
        )
      ],
    );
  }

  Widget listRestaurant(HomeFbBloC bloCFb) {
    return StreamBuilder<List<ModelEvent.Event>>(
        stream: bloCFb.lists,
        builder: (context, snapshot) {
          return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (context, index) => GestureDetector(
                    onDoubleTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => HotelDetailScreen(
                                    indexD: index,
                                    listsDetail: snapshot.data,
                                  )));
                    },
                    child: Card(
                      elevation: 0,
                      child: Container(
                        padding: EdgeInsets.all(16),
                        width: 382,
                        height: 259,
                        decoration: BoxDecoration(shape: BoxShape.rectangle),
                        child: Column(
                          children: [
                            Container(
                              width: 350,
                              height: 162,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(16),
                                image: DecorationImage(
                                  image: NetworkImage(
                                      snapshot.data[index].imagePath),
                                  fit: BoxFit.cover,
                                ),
                              ),
                              child: Stack(children: [
                                Positioned(
                                  right: 8,
                                  top: 8,
                                  child: TextButton(
                                    child: Container(
                                      height: 35,
                                      width: 35,
                                      child: Icon(
                                        snapshot.data[index].save
                                            ? Icons.bookmark
                                            : Icons.bookmark_border_rounded,
                                        color: Colors.white,
                                        size: 25,
                                      ),
                                      decoration: BoxDecoration(
                                          color: Colors.transparent,
                                          borderRadius:
                                              BorderRadius.circular(50),
                                          border:
                                              Border.all(color: Colors.white)),
                                    ),
                                    onPressed: () {
                                      bloCFb.setSave(index);
                                    },
                                  ),
                                ),
                              ]),
                            ),
                            SizedBox(
                              height: 18,
                            ),
                            Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    snapshot.data[index].name,
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w600,
                                        color: Colors.black),
                                  ),
                                  Column(
                                    children: [
                                      Row(
                                        children: [
                                          Image.asset(
                                            "images/star.png",
                                            width: 100,
                                            height: 24,
                                          ),
                                          Text(snapshot.data[index].rating)
                                        ],
                                      ),
                                    ],
                                  )
                                ]),
                            SizedBox(
                              height: 6,
                            ),
                            InkWell(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => GoogleMapScreen(
                                                address: snapshot
                                                    .data[index].address,
                                              )));
                                },
                                child: Row(
                                  children: [
                                    Icon(
                                      Icons.location_on_outlined,
                                      size: 14,
                                    ),
                                    Text(snapshot.data[index].address,
                                        style: TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w400,
                                            color: Color(0xff9a9a9a))),
                                  ],
                                )),
                          ],
                        ),
                      ),
                    ),
                  ));
        });
  }
}
