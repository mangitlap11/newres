import 'dart:io';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:resapp/bloc/add_restaurant_bloc.dart';
import 'package:provider/provider.dart';
import '../home_screen.dart';
import 'package:dotted_border/dotted_border.dart';

class AddHotelScreen extends StatefulWidget {
  final dynamic data;
  const AddHotelScreen({this.data});

  @override
  _AddHotelScreenState createState() => _AddHotelScreenState();
}

class _AddHotelScreenState extends State<AddHotelScreen> {
  TextEditingController nameController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  final ref = FirebaseDatabase.instance.reference().child("List Restaurant");
  final refDetail =
      FirebaseDatabase.instance.reference().child("Restaurant Detail");

  @override
  Widget build(BuildContext context) {
    var bloC = context.read<AddRestaurantBloC>();
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Text(
            "Add Hotel",
            style: TextStyle(
                fontSize: 20, fontWeight: FontWeight.w600, color: Colors.black),
          ),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          )),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 32),
        child: SingleChildScrollView(
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: 896,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 22,
                ),
                Text(
                  "Hotel Name",
                  style: TextStyle(fontWeight: FontWeight.w600, fontSize: 14),
                ),
                SizedBox(
                  height: 8,
                ),
                hotelName(),
                SizedBox(
                  height: 8,
                ),
                Text(
                  "Do not exceed 40 characters when entering.",
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      color: Colors.grey),
                ),
                SizedBox(
                  height: 16,
                ),
                Text(
                  "Address",
                  style: TextStyle(fontWeight: FontWeight.w600, fontSize: 14),
                ),
                SizedBox(
                  height: 8,
                ),
                address(),
                SizedBox(
                  height: 16,
                ),
                Text(
                  "Description",
                  style: TextStyle(fontWeight: FontWeight.w600, fontSize: 14),
                ),
                SizedBox(
                  height: 8,
                ),
                description(),
                SizedBox(
                  height: 16,
                ),
                Text(
                  "Hotel image",
                  style: TextStyle(fontWeight: FontWeight.w600, fontSize: 14),
                ),
                SizedBox(
                  height: 8,
                ),
                button(bloC),
                SizedBox(height: 125),
                twobtn(bloC)
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget hotelName() {
    return TextFormField(
      controller: nameController,
      onChanged: (value) => print(value),
      style: TextStyle(
          fontWeight: FontWeight.w400, fontSize: 14, color: Color(0xff1a1a1a)),
      decoration: InputDecoration(
        errorText: null,
        hintStyle: TextStyle(fontWeight: FontWeight.w400, fontSize: 14),
        hintText: "Input for hotel name ",
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
        fillColor: Color(0xfff4f5f8),
        filled: true,
      ),
      keyboardType: TextInputType.emailAddress,
      obscureText: false,
      onEditingComplete: () {
        FocusScope.of(context).nextFocus();
      },
    );
  }

  Widget address() {
    return TextFormField(
      controller: addressController,
      onChanged: (value) => print(value),
      style: TextStyle(
          fontWeight: FontWeight.w400, fontSize: 14, color: Color(0xff1a1a1a)),
      decoration: InputDecoration(
        errorText: null,
        hintStyle: TextStyle(fontWeight: FontWeight.w400, fontSize: 14),
        hintText: "Input for adress ",
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
        fillColor: Color(0xfff4f5f8),
        filled: true,
      ),
      keyboardType: TextInputType.emailAddress,
      obscureText: false,
      onEditingComplete: () {
        FocusScope.of(context).nextFocus();
      },
    );
  }

  Widget description() {
    return TextFormField(
      controller: descriptionController,
      onChanged: (value) => print(value),
      style: TextStyle(
          fontWeight: FontWeight.w400, fontSize: 14, color: Color(0xff1a1a1a)),
      decoration: InputDecoration(
        errorText: null,
        hintStyle: TextStyle(fontWeight: FontWeight.w400, fontSize: 14),
        hintText: "Input for description ",
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
        fillColor: Color(0xfff4f5f8),
        filled: true,
      ),
      keyboardType: TextInputType.emailAddress,
      obscureText: false,
      onEditingComplete: () {
        FocusScope.of(context).nextFocus();
      },
    );
  }

  Widget button(AddRestaurantBloC bloC) {
    return StreamBuilder<File>(
        stream: bloC.image,
        builder: (context, snapshot) {
          return TextButton(
            child: snapshot.data != null
                ? Container(
                    width: MediaQuery.of(context).size.width,
                    height: 160,
                    decoration: BoxDecoration(
                      image: snapshot.data != null
                          ? DecorationImage(
                              image: FileImage(snapshot.data),
                              fit: BoxFit.cover)
                          : null,
                      borderRadius: BorderRadius.circular(8),
                    ),
                  )
                : DottedBorder(
                    color: Colors.grey,
                    strokeWidth: 1,
                    child: Container(
                      height: 139,
                      width: 139,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.crop_original_rounded,
                            size: 20,
                            color: Colors.grey,
                          ),
                          Text(
                            "Add image",
                            style: TextStyle(
                                color: Color(0xff9a9a9a),
                                fontSize: 13,
                                fontWeight: FontWeight.w400),
                          ),
                        ],
                      ),
                      decoration: BoxDecoration(
                        image: snapshot.data != null
                            ? DecorationImage(
                                image: FileImage(snapshot.data),
                                fit: BoxFit.cover)
                            : null,
                        borderRadius: BorderRadius.circular(8),
                        color: Colors.white,
                      ),
                    ),
                  ),
            onPressed: () {
              bloC.getImagefromGallary();
            },
          );
        });
  }

  Widget twobtn(AddRestaurantBloC bloC) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 53,
      child: Row(
        children: [
          TextButton(
              style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.white),
                  shape: MaterialStateProperty.all(RoundedRectangleBorder(
                    side: BorderSide(color: Colors.grey),
                    borderRadius: BorderRadius.circular(8),
                  ))),
              onPressed: () {
                Navigator.pop(context);
              },
              child: Container(
                width: 95,
                height: 53,
                child: Center(
                  child: Text(
                    'Cancel',
                    style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 14,
                        color: Colors.grey),
                  ),
                ),
              )),
          SizedBox(
            width: 10.3,
          ),
          TextButton(
              style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.blue),
                  shape: MaterialStateProperty.all(RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ))),
              onPressed: () async {
                await bloC.uploadFile();
                String downloadURL = await bloC.getUrl();
                print(downloadURL);
                bloC.pushData(nameController, addressController,
                    descriptionController, downloadURL);

                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => HomeScreen(
                            data: widget.data,
                          )),
                );
              },
              child: Container(
                width: 210,
                height: 53,
                child: Center(
                  child: Text(
                    'Done',
                    style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 14,
                        color: Colors.white),
                  ),
                ),
              )),
        ],
      ),
    );
  }
}
