import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:resapp/bloc/googlemap_bloc.dart';

class GoogleMapScreen extends StatefulWidget {
  final String address;
  GoogleMapScreen({this.address});

  @override
  _GoogleMapScreenState createState() => _GoogleMapScreenState();
}

class _GoogleMapScreenState extends State<GoogleMapScreen> {
  GoogleMapController _myController;
  double lat;
  double long;

  @override
  void initState() {
    super.initState();

    var bloC = context.read<GMBloC>();
    bloC.onMapCreated(_myController);
    bloC.getLocation(widget.address).then((value) {
      setState(() {
        lat = value[0]["latitude"];
        long = value[0]["longitude"];
        bloC.onAddMarker(lat, long);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    var bloC = context.read<GMBloC>();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        title: Text(
          "Hotel Address",
          style: TextStyle(
              fontSize: 20, fontWeight: FontWeight.w600, color: Colors.black),
        ),
        centerTitle: true,
        leading: InkWell(
          child: Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
          onTap: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: StreamBuilder<Set<Marker>>(
          stream: bloC.maker,
          builder: (context, snapshot) {
            return GoogleMap(
              markers: snapshot.data,
              mapType: MapType.normal,
              onMapCreated: bloC.onMapCreated,
              initialCameraPosition:
                  CameraPosition(target: LatLng(lat, long), zoom: 15),
            );
          }),
    );
  }
}
