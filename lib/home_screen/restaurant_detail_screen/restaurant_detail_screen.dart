import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:resapp/bloc/detail_restaurant_bloc.dart';
import 'package:resapp/bloc/home_fb_bloc.dart';
import 'package:resapp/home_screen/googlemap_screen/googlemap_screen.dart';
import 'package:provider/provider.dart';
import 'package:resapp/model/list_restaurant_model.dart' as ModelEvent;
import 'package:resapp/model/review_model.dart';

class HotelDetailScreen extends StatefulWidget {
  final int indexD;
  final List<ModelEvent.Event> listsDetail;
  HotelDetailScreen({this.listsDetail, this.indexD});

  @override
  _HotelDetailScreenState createState() => _HotelDetailScreenState();
}

class _HotelDetailScreenState extends State<HotelDetailScreen> {
  final ref = FirebaseDatabase.instance.reference().child("Restaurant Detail");

  @override
  void initState() {
    super.initState();
    var bloCDetail = context.read<DetailBloC>();
    bloCDetail.getReview(widget.listsDetail, widget.indexD);
  }

  @override
  Widget build(BuildContext context) {
    var bloCFb = context.watch<HomeFbBloC>();
    var bloCDetail = context.read<DetailBloC>();
    return Scaffold(
      backgroundColor: Colors.white,
      body: ListView(children: [
        Column(children: [
          SizedBox(
              width: MediaQuery.of(context).size.width,
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: Stack(
                  children: [
                    image(context, bloCFb),
                    detail(context, bloCDetail)
                  ],
                ),
              ))
        ])
      ]),
    );
  }

  Widget image(BuildContext context, HomeFbBloC bloCFb) {
    return Row(
      children: [
        Container(
            height: 300,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              image: DecorationImage(
                image:
                    NetworkImage(widget.listsDetail[widget.indexD].imagePath),
                fit: BoxFit.cover,
              ),
              borderRadius: BorderRadius.circular(0),
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Stack(alignment: Alignment.topCenter, children: [
                Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          children: [
                            IconButton(
                              icon: Icon(
                                Icons.arrow_back,
                                color: Colors.white,
                              ),
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            ),
                          ],
                        ),
                        Column(
                          children: [
                            Row(
                              children: [
                                InkWell(
                                    onTap: () {},
                                    child: Image.asset(
                                      "images/share.png",
                                      width: 25,
                                      height: 25,
                                    )),
                                SizedBox(
                                  width: 13,
                                ),
                                InkWell(
                                  child: Icon(
                                    widget.listsDetail[widget.indexD].save
                                        ? Icons.bookmark
                                        : Icons.bookmark_border,
                                    color: Colors.white,
                                    size: 35,
                                  ),
                                  onTap: () {
                                    bloCFb.setSave(widget.indexD);
                                  },
                                ),
                              ],
                            ),
                          ],
                        )
                      ],
                    ),
                  ],
                ),
              ]),
            )),
      ],
    );
  }

  detail(BuildContext context, DetailBloC bloCDetail) {
    return Positioned(
        top: 260,
        child: Container(
            alignment: Alignment.bottomCenter,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(25),
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 28,
                    ),
                    Text(
                      widget.listsDetail[widget.indexD].name,
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Row(
                      children: [
                        Image.asset(
                          "images/star.png",
                          width: 100,
                          height: 24,
                        ),
                        Text(widget.listsDetail[widget.indexD].rating),
                        Text("(1.2k reviews)")
                      ],
                    ),
                    SizedBox(
                      height: 11,
                    ),
                    InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => GoogleMapScreen(
                                      address: widget.listsDetail[widget.indexD]
                                          .address)));
                        },
                        child: Row(
                          children: [
                            Icon(
                              Icons.location_on_outlined,
                              size: 14,
                            ),
                            Text(widget.listsDetail[widget.indexD].address,
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    color: Color(0xff9a9a9a))),
                          ],
                        )),
                    SizedBox(
                      height: 29,
                    ),
                    Text(
                      "Description",
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                          color: Colors.blue),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    StreamBuilder<bool>(
                        stream: bloCDetail.more,
                        builder: (context, snapshot) {
                          return Text(
                            widget.listsDetail[widget.indexD].description,
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.w400),
                            maxLines: snapshot.data ? 50 : 3,
                          );
                        }),
                    SizedBox(
                      height: 28,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Reviews",
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                              color: Colors.blue),
                        ),
                        InkWell(
                          onTap: () {
                            bloCDetail.setMore();
                          },
                          child: Text(
                            "See more",
                            style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                decoration: TextDecoration.underline),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    StreamBuilder<Review>(
                        stream: bloCDetail.review,
                        builder: (context, snapshot) {
                          return Card(
                              elevation: 0,
                              child: Column(
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Column(
                                        children: [
                                          Row(
                                            children: [
                                              Column(
                                                children: [
                                                  CircleAvatar(
                                                    backgroundImage:
                                                        NetworkImage(snapshot
                                                            .data.avatar),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                width: 12,
                                              ),
                                              Column(
                                                children: [
                                                  Text(snapshot.data.name),
                                                  Image.asset("images/star.png",
                                                      width: 56, height: 16),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                      Column(
                                        children: [
                                          Row(
                                            children: [
                                              Text(snapshot.data.like),
                                              SizedBox(
                                                width: 12,
                                              ),
                                              Icon(
                                                Icons.thumb_up,
                                                color: Colors.grey,
                                              )
                                            ],
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                  SizedBox(
                                    height: 16,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        snapshot.data.comment,
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ],
                                  )
                                ],
                              ));
                        }),
                  ],
                ),
              ),
            )));
  }
}
