import 'package:flutter/material.dart';
import 'package:resapp/bloc/login_bloc.dart';
import 'package:resapp/home_screen/home_screen.dart';
import 'package:resapp/login_screen/dialog/dialog.dart';
import 'package:resapp/login_screen/dialog/message_dialog.dart';
import 'package:resapp/model/model_api.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var bloC = context.read<LoginBloC>();

    return LayoutBuilder(
        builder: (context, constraints) => Scaffold(
            backgroundColor: Color(0xffFAFBFF),
            body: Padding(
              padding: EdgeInsets.symmetric(horizontal: 32),
              child: SingleChildScrollView(
                child: Container(
                  alignment: Alignment.center,
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      image(),
                      SizedBox(
                        height: 80,
                      ),
                      text(),
                      SizedBox(
                        height: 60,
                      ),
                      email(bloC),
                      SizedBox(
                        height: 16,
                      ),
                      password(bloC),
                      SizedBox(
                        height: 40,
                      ),
                      btnSignIn(constraints, bloC)
                    ],
                  ),
                ),
              ),
            )));
  }

  Widget image() {
    return Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(
                  "images/logo.png",
                ),
                fit: BoxFit.cover)),
        width: 175,
        height: 117,
        child: Stack(children: [
          Positioned(
              right: 10,
              left: 12,
              top: 17,
              child: Image.asset(
                "images/buildings.png",
                width: 100,
                height: 100,
              ))
        ]));
  }

  Widget text() {
    return Column(
      children: [
        Text("Welcome to Condotel!",
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600)),
        SizedBox(
          height: 4,
        ),
        Text("Alive with your style of living!",
            style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w400,
                color: Color(0xff9a9a9a)))
      ],
    );
  }

  Widget email(LoginBloC bloC) {
    return TextFormField(
      controller: emailController,
      onChanged: (value) => bloC.email = value,
      style: TextStyle(
          fontWeight: FontWeight.w400, fontSize: 14, color: Color(0xff1a1a1a)),
      decoration: InputDecoration(
        errorText: null,
        hintStyle: TextStyle(fontWeight: FontWeight.w400, fontSize: 14),
        hintText: "Email ",
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
        fillColor: Color(0xfff4f5f8),
        filled: true,
      ),
      keyboardType: TextInputType.emailAddress,
      obscureText: false,
      onEditingComplete: () {
        FocusScope.of(context).nextFocus();
      },
    );
  }

  Widget password(LoginBloC bloC) {
    return StreamBuilder<bool>(
      initialData: false,
      stream: bloC.eye,
      builder: (context, snapshot) => TextFormField(
        controller: passwordController,
        onChanged: (value) => bloC.password = value,
        style: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 14,
            color: Color(0xff1a1a1a)),
        decoration: InputDecoration(
          errorText: null,
          hintStyle: TextStyle(fontWeight: FontWeight.w400, fontSize: 14),
          hintText: "Password",
          labelStyle: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 14,
              color: Color(0xff1a1a1a)),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
          suffixIcon: TextButton(
            child: Icon(
              Icons.remove_red_eye_outlined,
              color: Color(0xff9A9A9A),
            ),
            onPressed: () {
              bloC.hidePassword();
            },
          ),
          fillColor: Color(0xfff4f5f8),
          filled: true,
        ),
        keyboardType: TextInputType.text,
        obscureText: snapshot.data,
        onEditingComplete: () {
          FocusScope.of(context).nextFocus();
        },
      ),
    );
  }

  Widget btnSignIn(BoxConstraints constraints, LoginBloC bloC) {
    return Container(
        width: constraints.maxWidth,
        height: 56,
        child: TextButton(
          onPressed: () async {
            LoadingDialog.showLoadingDialog(context, 'LOADING...');
            await bloC
                .loginRequest(emailController.text, passwordController.text)
                .then((value) {
              if (value.success) {
                LoadingDialog.hideLoadingDialog(context);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => HomeScreen(
                              data: value.data,
                            )));
              } else {
                LoadingDialog.hideLoadingDialog(context);
                MsgDialog.showMsgDialog(context, "Sign-In",
                    "Your email or password is wrong! Check Again!!!");
              }
            });
          },
          child: Text(
            'Sign In',
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.w700, fontSize: 14),
            textAlign: TextAlign.center,
          ),
          style: ButtonStyle(
              shape: MaterialStateProperty.all(RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5))),
              backgroundColor: MaterialStateProperty.all(Color(0xff0057FF))),
        ));
  }
}
