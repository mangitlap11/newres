import 'package:flutter/material.dart';

import '../login_screen.dart';

class MsgDialog {
  static void showMsgDialog(BuildContext context, String title, String msg) {
    showDialog(
        context: context,
        builder: (context) => Container(
              height: 100,
              child: AlertDialog(
                title: Text(
                  title,
                ),
                content: Text(msg),
                actions: [
                  OutlinedButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text('OK'))
                ],
              ),
            ));
  }
}
